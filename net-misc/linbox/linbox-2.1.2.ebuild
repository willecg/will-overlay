# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( python3_{6,7} )

inherit meson

DESCRIPTION="Herramienta para realizar desconexiones seguras de RouterOs"
HOMEPAGE="https://www.cpapaseo.com"
SRC_URI="https://gitlab.com/willecg/${PN}/-/archive/${PV}/${P}.tar.bz2"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="${PYTHON_DEPS}
        >=x11-libs/gtk+-3.23.1:3[introspection]
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
    meson_src_install
}
